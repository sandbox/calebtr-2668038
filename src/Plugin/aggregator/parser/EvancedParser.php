<?php

/**
 * @file
 * Contains \Drupal\aggregator\Plugin\aggregator\parser\DefaultParser.
 */

namespace Drupal\evanced_aggregator\Plugin\aggregator\parser;

use Drupal\aggregator\Plugin\ParserInterface;
use Drupal\aggregator\FeedInterface;
use Drupal\aggregator\Plugin\aggregator\parser\DefaultParser;
use Zend\Feed\Reader\Reader;
use Zend\Feed\Reader\Exception\ExceptionInterface;

/**
 * Defines the Evanced aggregator parser implementation.
 *
 * Parses RSS, Atom and RDF feeds.
 *
 * @AggregatorParser(
 *   id = "evanced_aggregator",
 *   title = @Translation("Evanced parser"),
 *   description = @Translation("Evanced parser for RSS, Atom and RDF feeds.")
 * )
 */
class EvancedParser extends DefaultParser implements ParserInterface {

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed) {

    // Set our bridge extension manager to Zend Feed.
    Reader::setExtensionManager(\Drupal::service('feed.bridge.reader'));
    try {
      $channel = Reader::importString($feed->source_string);
    }
    catch (ExceptionInterface $e) {
      watchdog_exception('aggregator', $e);
      drupal_set_message(t('The feed from %site seems to be broken because of error "%error".', array('%site' => $feed->label(), '%error' => $e->getMessage())), 'error');

      return FALSE;
    }

    $feed->setWebsiteUrl($channel->getLink());
    $feed->setDescription($channel->getDescription());
    if ($image = $channel->getImage()) {
      $feed->setImage($image['uri']);
    }
    // Initialize items array.
    $feed->items = array();
    foreach ($channel as $item) {

      // Reset the parsed item.
      $parsed_item = array();

      // skip this item if one of the categories is "Staff development/meeting"
      $skip = false;
      $categories = $item->getCategories();
      $types = array();
      foreach ($categories as $category) {
        if ($category['term'] == "Staff development/meeting") {
          $skip = true;
        }
        $types[] = $category['term'];
      }
      if ($skip) {
        continue;
      }
      $parsed_item['eventtype'] = implode(' ', $types);

      // Move the values to an array as expected by processors.
      $parsed_item['title'] = $item->getTitle();
      $parsed_item['guid'] = $item->getId();
      $parsed_item['link'] = $item->getLink();

      // parse description into when and where; ignore description
      list($whenRaw, $where) = explode('<br>', $item->getDescription());
      $when = new \stdClass();
      $whenRaw = strip_tags($whenRaw);

      // strip the string 'when: ' from the beginning
      $whenRaw = substr($whenRaw, 6);

      // split into date, start and end
      $whenParts = explode(' - ', $whenRaw);
      if (!empty($whenParts[0])) {
        $whenDate = $whenParts[0];
      }
      if (!empty($whenParts[1])) {
        $whenStart = $whenParts[1];
      }
      if (!empty($whenParts[2])) {
        $whenEnd = $whenParts[2];
      }

      // process the date & overwrite timestamp
      $whenStamp = strtotime($whenDate);
      $parsed_item['timestamp'] = $whenStamp;

      // $when properties
      $when->month = date('M', $whenStamp);
      $when->day = date('j', $whenStamp);

      // process the times
      $whenStart = str_replace(array(' AM', ' PM'), '', $whenStart);
      $whenEnd = str_replace(array(' AM', ' PM'), '', $whenEnd);
      if (!empty($whenEnd)) {
        $when->time = $whenStart . ' - ' . $whenEnd;
      }
      else {
        $when->time = $whenStart;
      }

      $evancedItem = array(
        '#theme' => 'evanced_item',
        '#when' => $when,
        '#title' => $parsed_item['title'],
        '#link' => $parsed_item['link'],
      );

      // set the description to a render array and render it
      $parsed_item['description'] = \Drupal::service('renderer')->render($evancedItem);

      $parsed_item['author'] = '';
      if ($author = $item->getAuthor()) {
        $parsed_item['author'] = $author['name'];
      }
      // Store on $feed object. This is where processors will look for parsed items.
      $feed->items[] = $parsed_item;
    }

    return TRUE;
  }

}
